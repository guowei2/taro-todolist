import { View, Text, Input, Button, Checkbox, Label, Block } from '@tarojs/components'
import Taro, { useLoad } from '@tarojs/taro'
import { useImmer } from 'use-immer'
import { useEffect, useMemo, useState } from 'react'
import './index.scss'

interface TodoItem {
  id: number
  title: string
  completed: boolean
}

const STORAGE_KEY = 'vue-todomvc'

const filters = {
  all: (todos: TodoItem[]) => todos,
  active: (todos: TodoItem[]) => todos.filter((todo) => !todo.completed),
  completed: (todos: TodoItem[]) => todos.filter((todo) => todo.completed)
}

export default function Index() {
  const [addText, setAddText] = useState('')
  const [todos, setTodos] = useImmer<TodoItem[]>(Taro.getStorageSync(STORAGE_KEY) || [])
  const [visibility, setVisibility] = useState<keyof typeof filters>('all')
  const [editedTodo, setEditedTodo] = useImmer<TodoItem | null>(null)

  const remaining = useMemo(() => filters.active(todos).length, [todos])
  const filteredTodos = useMemo(() => filters[visibility](todos), [todos, visibility])

  function addTodo(): void {
    const value = addText.trim()
    if (value) {
      setTodos((draft) => {
        draft.push({
          id: Date.now(),
          title: value,
          completed: false
        })
      })
      setAddText('')
    }
  }

  function toggleAll() {
    const allChecked = todos.every((v) => v.completed)
    setTodos((draft) => {
      draft.forEach((todo) => (todo.completed = !allChecked))
    })
  }

  function editTodo(todo: TodoItem) {
    setEditedTodo(JSON.parse(JSON.stringify(todo)))
  }

  function removeTodo(todo: TodoItem) {
    setTodos((draft) => {
      draft.splice(
        draft.findIndex((v) => v.id === todo.id),
        1
      )
    })
  }

  function doneEdit(todo: TodoItem) {
    const title = editedTodo!.title.trim()
    if (title) {
      setTodos((draft) => {
        const item = draft.find((v) => v.id === todo.id)!
        item.title = title
      })
    } else removeTodo(todo)
    setEditedTodo(null)
  }

  function cancelEdit() {
    setEditedTodo(null)
  }

  function removeCompleted() {
    setTodos(filters.active(todos))
  }

  // 状态持久化
  useEffect(() => {
    Taro.setStorageSync(STORAGE_KEY, todos)
  }, [todos])

  function onSwitch(href: keyof typeof filters) {
    setVisibility(href)
  }

  useLoad(() => {
    console.log('Page loaded.')
  })

  return (
    <View className="todoapp">
      <View className="header">
        <View className="h1">Todos</View>
        <Input
          className="new-todo"
          placeholder-style="font-style: italic;font-weight: 400;color: rgba(0, 0, 0, 0.4);"
          auto-focus
          placeholder="What needs to be done?"
          value={addText}
          onInput={(e) => setAddText(e.detail.value)}
        />
        {addText && (
          <Button className="add-btn" size="mini" onClick={addTodo}>
            Add
          </Button>
        )}
      </View>
      <View className="main" hidden={!todos.length}>
        <Checkbox id="toggle-all" className="toggle-all" checked={remaining === 0} value="" />
        <Label for="toggle-all" onClick={toggleAll}>
          Mark all as complete
        </Label>
        <View className="todo-list">
          {filteredTodos.map((todo) => (
            <View
              className={`li todo ${todo.completed ? 'completed' : ''} ${
                todo.id === editedTodo?.id ? 'editing' : ''
              }`}
              key={todo.id}
            >
              <View className="view">
                <Checkbox
                  className="toggle"
                  checked={todo.completed}
                  onClick={() => {
                    setTodos((draft) => {
                      const v = draft.find((t) => t.id === todo.id)!
                      v.completed = !v.completed
                    })
                  }}
                  value=""
                />
                <View onLongPress={() => editTodo(todo)}>
                  {/* label当前版本不会触发onLongPress，故用view包裹 */}
                  <Label className={`label ${todo.completed ? 'label_checked' : ''}`}>
                    {todo.title}
                  </Label>
                </View>

                <Button className="destroy" onClick={() => removeTodo(todo)}></Button>
              </View>
              {todo.id === editedTodo?.id && (
                <Block>
                  <Input
                    className="edit"
                    type="text"
                    value={editedTodo.title}
                    onInput={(e) => {
                      setEditedTodo((draft) => {
                        draft!.title = e.detail.value
                      })
                    }}
                    auto-focus
                  />
                  <Button
                    className="confirm"
                    type="primary"
                    size="mini"
                    onClick={() => doneEdit(todo)}
                  >
                    确认
                  </Button>
                  <Button className="cancel" size="mini" onClick={cancelEdit}>
                    取消
                  </Button>
                </Block>
              )}
            </View>
          ))}
        </View>
      </View>
      <View className="footer" hidden={!todos.length}>
        <Text className="todo-count">
          <Text className="strong">{remaining}</Text>
          <Text> item{remaining === 1 ? '' : 's'} left</Text>
        </Text>
        <View className="filters">
          <View className="li">
            <Text
              className={`a ${visibility === 'all' ? 'selected' : ''}`}
              onClick={() => onSwitch('all')}
            >
              All
            </Text>
          </View>
          <View className="li">
            <Text
              className={`a ${visibility === 'active' ? 'selected' : ''}`}
              onClick={() => onSwitch('active')}
            >
              Active
            </Text>
          </View>
          <View className="li">
            <Text
              className={`a ${visibility === 'completed' ? 'selected' : ''}`}
              onClick={() => onSwitch('completed')}
            >
              Completed
            </Text>
          </View>
        </View>
        <Button
          className="clear-completed"
          size="mini"
          onClick={removeCompleted}
          hidden={todos.length <= remaining}
        >
          Clear completed
        </Button>
      </View>
    </View>
  )
}
