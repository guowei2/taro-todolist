# taro-todolist

Taro版todolist

## 开发体验

1. 和react开发体验基本一致，类型方面很完善，就更新数据等没vue方便
2. taro框架bug,label text组件上的onLongPress不生效 <https://github.com/NervJS/taro/issues/15256>
3. taro框架bug,单元测试无法运行，报错 <https://github.com/NervJS/taro-test-utils/issues/38>

## 开发注意

1. 全局安装czg ,之后提交都用czg命令，这样更规范
